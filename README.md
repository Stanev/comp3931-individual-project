# COMP3931 Individual Project

This repository contains the source files of the software prototype as part for my final year project.


Instructions to run:

`python -m venv venv` – Creates a virtual environment named “venv”

`source venv/bin/activate` – Activates the virtual environment for the terminal from which the command has been ran

`pip install -r requirements.txt` – This will download and install all the libraries and dependencies needed

`python manage.py makemigrations` (optional) – Creates new migrations based on changes made to the models file

`python manage.py migrate` (optional) – Creates the tables in the database file

`python manage.py runserver` – Activates the web server hosting the prototype. By default, it runs on localhost
