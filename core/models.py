from django.db import models
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField
from django.utils import *


# Models
class Doctor(models.Model):
    FirstName = models.CharField(max_length=256)
    LastName = models.CharField(max_length=256)
    PhoneNumber = PhoneNumberField(region='GB')
    Address = models.CharField(max_length=512)

    def __str__(self):
        return f'{self.FirstName} {self.LastName}'

    class Meta:
        verbose_name = 'Doctor'
        verbose_name_plural = 'Doctors'


class Nurse(models.Model):
    FirstName = models.CharField(max_length=256)
    LastName = models.CharField(max_length=256)
    PhoneNumber = PhoneNumberField(region='GB')
    Address = models.CharField(max_length=512)

    def __str__(self):
        return f'{self.FirstName} {self.LastName}'

    class Meta:
        verbose_name = 'Nurse'
        verbose_name_plural = 'Nurses'


class Patient(models.Model):
    FirstName = models.CharField(max_length=256)
    LastName = models.CharField(max_length=256)
    NHSNumber = models.CharField(max_length=256)
    PhoneNumber = PhoneNumberField(region='GB')
    Address = models.CharField(max_length=512)

    def __str__(self):
        return f'{self.FirstName} {self.LastName}'

    def get_absolute_url(self):
        return reverse('core:patient_detail', args=[str(self.id)])


    class Meta:
        verbose_name = 'Patient'
        verbose_name_plural = 'Patients'


class Appointment(models.Model):
    Time = models.DateTimeField()
    Doctor = models.ForeignKey(Doctor, on_delete=models.SET_NULL, null=True)
    Patient = models.ForeignKey(Patient, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f'{self.Time} {self.Doctor} {self.Patient}'

    class Meta:
        verbose_name = 'Appointment'
        verbose_name_plural = 'Appointments'


class Medicine(models.Model):
    Code = models.CharField(max_length=128)
    Name = models.CharField(max_length=256)
    Brand = models.CharField(max_length=256)
    Description = models.CharField(max_length=512)

    def __str__(self):
        return f'{self.Code} {self.Name}'

    class Meta:
        verbose_name = 'Medicine'
        verbose_name_plural = 'Medicines'


class Prescription(models.Model):
    Doctor = models.ForeignKey(Doctor, on_delete=models.SET_NULL, null=True)
    Patient = models.ForeignKey(Patient, on_delete=models.SET_NULL, null=True)
    Appointment = models.ForeignKey(Appointment, on_delete=models.SET_NULL, null=True)
    Medicine = models.ForeignKey(Medicine, on_delete=models.SET_NULL, null=True)
    Date = models.DateField()
    Dose = models.CharField(max_length=5)

    def __str__(self):
        return f'{self.Date} {self.Dose}'

    class Meta:
        verbose_name = 'Prescription'
        verbose_name_plural = 'Prescriptions'


class Note(models.Model):
    Doctor = models.ForeignKey(Doctor, on_delete=models.SET_NULL, null=True)
    Patient = models.ForeignKey(Patient, on_delete=models.SET_NULL, null=True)
    Note = models.TextField(max_length=5000)

    def __str__(self):
        return f'{self.Doctor} {self.Patient} {self.Note}'

    class Meta:
        verbose_name = 'Note'
        verbose_name_plural = 'Notes'


class Procedure(models.Model):
    Name = models.CharField(max_length=512)
    Doctor = models.ForeignKey(Doctor, on_delete=models.SET_NULL, null=True)
    Nurse = models.ForeignKey(Nurse, on_delete=models.SET_NULL, null=True)
    Patient = models.ForeignKey(Patient, on_delete=models.SET_NULL, null=True)
    Date = models.DateField()
    ProcedureID = models.CharField(max_length=128)

    def __str__(self):
        return f'{self.Name} {self.Date} {self.ProcedureID}'

    class Meta:
        verbose_name = 'Procedure'
        verbose_name_plural = 'Procedures'

