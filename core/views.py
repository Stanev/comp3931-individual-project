from django.shortcuts import render, redirect
from django.views.generic import UpdateView, DeleteView
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from .models import *
from .forms import *


# Create your views here.
def home(request):
    return render(request, 'core/index.html')


# Doctor related Views
def DoctorsView(request):
    obj = Doctor.objects.all()
    return render(request, 'core/doctors.html', {'obj': obj, })


def AddDoctor(request):
    title = "Add Doctor"
    if request.method == 'POST':
        form = DoctorForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('core:doctorslist')
    else:
        form = DoctorForm()
    context = {
        'form': form,
        'title': title
    }
    return render(request, 'core/add-edit.html', context)


class UpdateDoctor(UpdateView):
    form_class = DoctorForm
    model = Doctor
    template_name = 'core/add-edit.html'
    success_url = reverse_lazy('core:doctorslist')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        title = 'Update Doctor'
        context['title'] = title
        return context


class DeleteDoctor(DeleteView):
    model = Doctor
    success_url = reverse_lazy('core:doctorslist')


# Nurse related Views
def NursesView(request):
    obj = Nurse.objects.all()
    return render(request, 'core/nurse.html', {'obj': obj, })


def AddNurse(request):
    if request.method == 'POST':
        form = NurseForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('core:nurseslist')
    else:
        form = NurseForm()
    return render(request, 'core/add-edit.html', {'form': form, 'title': "Add Nurse"})


class UpdateNurse(UpdateView):
    form_class = PatientForm
    model = Patient
    template_name = 'core/add-edit.html'
    success_url = reverse_lazy('core:nurseslist')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        title = 'Update Nurse'
        context['title'] = title
        return context


class DeleteNurse(DeleteView):
    model = Nurse
    success_url = reverse_lazy('core:nurseslist')


# Patient related View
def PatientsView(request):
    obj = Patient.objects.all()
    return render(request, 'core/patient.html', {'obj': obj, })


def AddPatient(request):
    if request.method == 'POST':
        form = PatientForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('core:patientslist')
    else:
        form = PatientForm()
    return render(request, 'core/add-edit.html', {'form': form, 'title': "Add Patient"})


class UpdatePatient(UpdateView):
    form_class = PatientForm
    model = Patient
    template_name = 'core/add-edit.html'
    success_url = reverse_lazy('core:patientslist')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        title = 'Update Patient'
        context['title'] = title
        return context


class DeletePatient(DeleteView):
    model = Patient
    success_url = reverse_lazy('core:patientslist')


def PatientDetail(request, pk):
    patient = get_object_or_404(Patient, id=pk)
    prescriptions = Prescription.objects.filter(Patient=patient)
    notes = Note.objects.filter(Patient=patient)

    context = {
        'patient': patient,
        'prescription': prescriptions,
        'note': notes,
    }

    return render(request, 'core/patient_detail.html', context)


# Appointment related views
def AppointmentsView(request):
    obj = Appointment.objects.all()
    return render(request, 'core/appointments.html', {'obj': obj})


def AddAppointment(request):
    if request.method == 'POST':
        form = AppointmentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('core:appointmentslist')
    else:
        form = AppointmentForm()
    return render(request, 'core/add-edit.html', {'form': form, 'title': "Add Appointment"})


class UpdateAppointment(UpdateView):
    form_class = AppointmentForm
    model = Appointment
    template_name = 'core/add-edit.html'
    success_url = reverse_lazy('core:appointmentslist')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        title = 'Update Appointment'
        context['title'] = title
        return context


class DeleteAppointment(DeleteView):
    model = Appointment
    success_url = reverse_lazy('core:appointmentslist')


# prescription related views
def PrescriptionsView(request, pk):
    obj = Prescription.objects.filter(Patient_id=pk)


def AddPrescription(request):
    if request.method == 'POST':
        form = PrescriptionForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('core:patientslist')
    else:
        form = PrescriptionForm()

    return render(request, 'core/add-edit.html', {'form': form, 'title': "Add Precsription"})


# medicine related views
def MedicinesView(request):
    obj = Medicine.objects.all()
    return render(request, 'core/medicine.html', {'obj': obj})


def AddMedicine(request):
    if request.method == 'POST':
        form = MedicineForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('core:medicineslist')
    else:
        form = MedicineForm()
    return render(request, 'core/add-edit.html', {'form': form, 'title': "Add Medicine"})


class UpdateMedicine(UpdateView):
    form_class = MedicineForm
    model = Medicine
    template_name = 'core/add-edit.html'
    success_url = reverse_lazy('core:medicineslist')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        title = 'Update Medicine'
        context['title'] = title
        return context


class DeleteMedicine(DeleteView):
    model = Medicine
    success_url = reverse_lazy('core:medicineslist')


# notes related views
def NotesView(request, pk):
    obj = Note.objects.filter(Patient_id=pk)


def AddNote(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('core:patientslist')
    else:
        form = NoteForm()

    return render(request, 'core/add-edit.html', {'form': form, 'title': "Add Note"})


class UpdateNote(UpdateView):
    form_class = MedicineForm
    model = Medicine
    template_name = 'core/add-edit.html'
    success_url = reverse_lazy('core:patientslist')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        title = 'Update Note'
        context['title'] = title
        return context


class DeleteNote(DeleteView):
    model = Note
    success_url = reverse_lazy('core:patientslist')


# Procedure related views
def ProceduresView(request):
    obj = Procedure.objects.all()
    return render(request, 'core/procedure.html', {'obj': obj, 'title': "Procedures"})


def AddProcedure(request):
    if request.method == 'POST':
        form = ProcedureForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('core:procedureslist')
    else:
        form = ProcedureForm()

    return render(request, 'core/add-edit.html', {'form': form, 'title': "Add Procedure"})


class UpdateProcedure(UpdateView):
    form_class = ProcedureForm
    model = Procedure
    template_name = 'core/add-edit.html'
    success_url = reverse_lazy('core:procedureslist')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        title = 'Update Procedure'
        context['title'] = title
        return context


class DeleteProcedure(DeleteView):
    model = Procedure
    success_url = reverse_lazy('core:procedureslist')
