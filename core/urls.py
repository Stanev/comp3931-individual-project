from django.urls import path
from .views import *
app_name = 'core'

urlpatterns = [
    path('', home, name='home'),
    # Doctor related urls
    path('doctorslist/', DoctorsView, name='doctorslist'),
    path('adddoctor/', AddDoctor, name='adddoctor'),
    path('updatedoctor/<int:pk>/', UpdateDoctor.as_view(), name='updatedoctor'),
    path('deletedoctor/<int:pk>/', DeleteDoctor.as_view(), name='deletedoctor'),
    # nurse related urls
    path('nurseslist/', NursesView, name='nurseslist'),
    path('addnurse/', AddNurse, name='addnurse'),
    path('updatenurse/<int:pk>/', UpdateNurse.as_view(), name='updatenurse'),
    path('deletenurse/<int:pk>/', DeleteNurse.as_view(), name='deletenurse'),
    # patient related urls
    path('patientslist/', PatientsView, name='patientslist'),
    path('addpatient/', AddPatient, name='addpatient'),
    path('updatepatient/<int:pk>/', UpdatePatient.as_view(), name='updatepatient'),
    path('deletepatient/<int:pk>/', DeletePatient.as_view(), name='deletepatient'),
    path('patientdetail/<int:pk>/', PatientDetail, name='patientdetail'),
    # appointment related urls
    path('appointmentslist/', AppointmentsView, name='appointmentslist'),
    path('addappointment/', AddAppointment, name='addappointment'),
    path('updateappointment/<int:pk>/', UpdateAppointment.as_view(), name='updateappointment'),
    path('deleteappointment/<int:pk>/', DeleteAppointment.as_view(), name='deleteappointment'),
    # prescription related urls
    path('prescriptionview/<int:pk>', PrescriptionsView, name='prescriptionview'),
    path('addprescription/', AddPrescription, name='addprescription'),
    # medicine related urls
    path('medicineslist/', MedicinesView, name='medicineslist'),
    path('addmedicine/', AddMedicine, name='addmedicine'),
    path('updatemedicine/<int:pk>/', UpdateMedicine.as_view(), name='updatemedicine'),
    path('deletemedicine/<int:pk>/', DeleteMedicine.as_view(), name='deletemedicine'),
    # note related urls
    path('notesview/<int:pk>/', NotesView, name='notesview'),
    path('addnote/', AddNote, name='addnote'),
    path('updatenote/<int:pk>/', UpdateNote.as_view(), name='updatenote'),
    path('deletenote/<int:pk>/', DeleteNote.as_view(), name='deletenote'),
    # procedure related urls
    path('procedureslist/', ProceduresView, name='procedureslist'),
    path('addprocedure/', AddProcedure, name='addprocedure'),
    path('updateprocedure/<int:pk>/', UpdateProcedure.as_view(), name='updateprocedure'),
    path('deleteprocedure/<int:pk>/', DeleteProcedure.as_view(), name='deleteprocedure'),
]
