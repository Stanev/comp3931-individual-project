from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='Doctor',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.doctor'),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='Patient',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.patient'),
        ),
        migrations.AlterField(
            model_name='note',
            name='Doctor',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.doctor'),
        ),
        migrations.AlterField(
            model_name='note',
            name='Patient',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.patient'),
        ),
        migrations.AlterField(
            model_name='prescription',
            name='Appointment',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.appointment'),
        ),
        migrations.AlterField(
            model_name='prescription',
            name='Doctor',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.doctor'),
        ),
        migrations.AlterField(
            model_name='prescription',
            name='Medicine',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.medicine'),
        ),
        migrations.AlterField(
            model_name='prescription',
            name='Patient',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.patient'),
        ),
    ]
