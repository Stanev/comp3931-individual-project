from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20210616_1643'),
    ]

    operations = [
        migrations.CreateModel(
            name='Procedure',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Name', models.CharField(max_length=512)),
                ('Date', models.DateField()),
                ('Cost', models.CharField(max_length=128)),
                ('Doctor', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.doctor')),
                ('Nurse', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.nurse')),
                ('Patient', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.patient')),
            ],
            options={
                'verbose_name': 'Procedure',
                'verbose_name_plural': 'Procedures',
            },
        ),
    ]
