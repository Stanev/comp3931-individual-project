from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_rename_cost_procedure_procedureid'),
    ]

    operations = [
        migrations.RenameField(
            model_name='patient',
            old_name='Insurance',
            new_name='NHSNumber',
        ),
    ]
