# Generated by Django 3.2.4 on 2021-06-20 12:48

from django.db import migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_rename_insurance_patient_nhsnumber'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctor',
            name='PhoneNumber',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=128, region='GB'),
        ),
        migrations.AlterField(
            model_name='nurse',
            name='PhoneNumber',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=128, region='GB'),
        ),
        migrations.AlterField(
            model_name='patient',
            name='PhoneNumber',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=128, region='GB'),
        ),
    ]
