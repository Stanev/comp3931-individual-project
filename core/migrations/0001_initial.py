from django.db import migrations, models
import django.db.models.deletion
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Time', models.DateTimeField()),
            ],
            options={
                'verbose_name': 'Appointment',
                'verbose_name_plural': 'Appointments',
            },
        ),
        migrations.CreateModel(
            name='Doctor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('FirstName', models.CharField(max_length=256)),
                ('LastName', models.CharField(max_length=256)),
                ('PhoneNumber', phonenumber_field.modelfields.PhoneNumberField(max_length=128, region=None)),
                ('Address', models.CharField(max_length=512)),
            ],
            options={
                'verbose_name': 'Doctor',
                'verbose_name_plural': 'Doctors',
            },
        ),
        migrations.CreateModel(
            name='Medicine',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Code', models.CharField(max_length=128)),
                ('Name', models.CharField(max_length=256)),
                ('Brand', models.CharField(max_length=256)),
                ('Description', models.CharField(max_length=512)),
            ],
            options={
                'verbose_name': 'Medicine',
                'verbose_name_plural': 'Medicines',
            },
        ),
        migrations.CreateModel(
            name='Nurse',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('FirstName', models.CharField(max_length=256)),
                ('LastName', models.CharField(max_length=256)),
                ('PhoneNumber', phonenumber_field.modelfields.PhoneNumberField(max_length=128, region=None)),
                ('Address', models.CharField(max_length=512)),
            ],
            options={
                'verbose_name': 'Nurse',
                'verbose_name_plural': 'Nurses',
            },
        ),
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('FirstName', models.CharField(max_length=256)),
                ('LastName', models.CharField(max_length=256)),
                ('Insurance', models.CharField(max_length=256)),
                ('PhoneNumber', phonenumber_field.modelfields.PhoneNumberField(max_length=128, region=None)),
                ('Address', models.CharField(max_length=512)),
            ],
            options={
                'verbose_name': 'Patient',
                'verbose_name_plural': 'Patients',
            },
        ),
        migrations.CreateModel(
            name='Prescription',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Date', models.DateField()),
                ('Dose', models.CharField(max_length=5)),
                ('Appointment', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='core.appointment')),
                ('Doctor', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='core.doctor')),
                ('Medicine', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='core.medicine')),
                ('Patient', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='core.patient')),
            ],
            options={
                'verbose_name': 'Prescription',
                'verbose_name_plural': 'Prescriptions',
            },
        ),
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Note', models.TextField(max_length=5000)),
                ('Doctor', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='core.doctor')),
                ('Patient', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='core.patient')),
            ],
            options={
                'verbose_name': 'Note',
                'verbose_name_plural': 'Notes',
            },
        ),
        migrations.AddField(
            model_name='appointment',
            name='Doctor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='core.doctor'),
        ),
        migrations.AddField(
            model_name='appointment',
            name='Patient',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='core.patient'),
        ),
    ]
