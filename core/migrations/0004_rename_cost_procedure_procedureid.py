from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_procedure'),
    ]

    operations = [
        migrations.RenameField(
            model_name='procedure',
            old_name='Cost',
            new_name='ProcedureID',
        ),
    ]
