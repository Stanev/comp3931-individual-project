from django.contrib import admin
from .models import *
# Registration of models
admin.site.register(Doctor)
admin.site.register(Nurse)
admin.site.register(Patient)
admin.site.register(Medicine)
admin.site.register(Appointment)
admin.site.register(Prescription)
admin.site.register(Note)
admin.site.register(Procedure)
