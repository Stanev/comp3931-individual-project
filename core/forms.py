from django.forms import ModelForm, DateTimeInput, DateInput
from .models import *


class DoctorForm(ModelForm):
    class Meta:
        model = Doctor
        fields = '__all__'


class NurseForm(ModelForm):
    class Meta:
        model = Nurse
        fields = '__all__'


class PatientForm(ModelForm):
    class Meta:
        model = Patient
        fields = '__all__'


class MedicineForm(ModelForm):
    class Meta:
        model = Medicine
        fields = '__all__'


class AppointmentForm(ModelForm):
    class Meta:
        model = Appointment
        fields = '__all__'
        widgets = {
            'Time': DateTimeInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%d %H:%M')
        }


class PrescriptionForm(ModelForm):
    class Meta:
        model = Prescription
        fields = '__all__'
        widgets = {
            'Date': DateInput(attrs={'type': 'date'}, format='%Y-%m-%d')
        }


class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = '__all__'


class ProcedureForm(ModelForm):
    class Meta:
        model = Procedure
        fields = '__all__'
        widgets = {
            'Date': DateInput(attrs={'type': 'date'}, format='%Y-%m-%d')
        }
